rdforms.spec({
  language: document.targetLanguage,
  namespaces: {
    'brick': 'https://brickschema.org/schema/Brick#',
    'geo': 'http://www.w3.org/2003/01/geo/wgs84_pos#',
    'qudt': 'http://qudt.org/schema/qudt/'
  },
  bundles: [
    ['../energy0.9.json'],
  ],
  main: [
    'undefined-32',
    'undefined-28',
    'undefined-73',
    'undefined-74',
    'undefined-75',
  ],
  supportive: [
  ]
});